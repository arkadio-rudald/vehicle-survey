package com.arkadiusz.vehiclesurvey;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.AggregateCalculation;
import com.arkadiusz.vehiclesurvey.analysis.CalculationReport;
import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.helpers.DataCombiner;
import com.arkadiusz.vehiclesurvey.helpers.IOConnector;
import com.arkadiusz.vehiclesurvey.helpers.RecordParser;
import com.arkadiusz.vehiclesurvey.model.*;

import java.util.*;

class VehicleSurveyController {
    private String sourceFilePath;
    private Interval[] aggregationInterval = {Interval.HOUR};

    VehicleSurveyController(String sourceFilePath) {
        this.sourceFilePath = sourceFilePath;
    }

    public void startAnalyzing(Interval[] intervals,
                               List<ICalculation> iCalculations,
                               List<AggregateCalculation> aggregateCalculations,
                               String directoryPath,
                               List<String> filenames)
    {

        DataCombiner dataCombiner = new DataCombiner(new RecordParser(), sourceFilePath);
        Map<UniqueMapKey, Object> results = dataCombiner
                .iterateByIntervals(new ArrayList<>(Arrays.asList(intervals)),iCalculations);

        Map<UniqueMapKey, Object> aggregationResults = dataCombiner
                .analyseAggregationFunctions(aggregateCalculations, results, aggregationInterval);

        CalculationReport calculationReport = new CalculationReport(
                intervals,
                getClassTypes(iCalculations),
                getClassTypes(aggregateCalculations),
                dataCombiner.getDaysFromData(results),
                Direction.values()
        );


        IOConnector ioConnector = new IOConnector();
        ioConnector.saveReportsInDirectory(
                directoryPath,
                filenames,
                calculationReport.report(results,aggregationResults)
        );
    }

    private List<Class> getClassTypes(List iCalculations)
    {
        List<Class> calcClasses = new ArrayList<>();
        iCalculations.forEach(item -> calcClasses.add(item.getClass()));
        return calcClasses;
    }
}
