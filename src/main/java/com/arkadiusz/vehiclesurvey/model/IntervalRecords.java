package com.arkadiusz.vehiclesurvey.model;

import java.time.LocalTime;
import java.util.List;

public class IntervalRecords extends SessionPeriod{
    private List<VehicleRecord> vehicleRecords;

    public IntervalRecords(LocalTime startTime, LocalTime endTime, Day day, List<VehicleRecord> vehicleRecords) {
        super(startTime,endTime,day);
        this.vehicleRecords = vehicleRecords;
    }

    public LocalTime getStartTime() {
        return super.getStartTime();
    }

    public LocalTime getEndTime() {
        return super.getEndTime();
    }

    public Day getDay() {
        return super.getDay();
    }

    public List<VehicleRecord> getVehicleRecords() {
        return vehicleRecords;
    }
}
