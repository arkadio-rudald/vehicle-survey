package com.arkadiusz.vehiclesurvey.model;

public enum Interval {
    FIFTEEN_MINUTES(15),
    TWENTY_MINUTES(20),
    THIRTY_MINUTES(30),
    HOUR(60),
    TWELVE_HOURS(12*60),
    DAILY(24*60),
    WEEKLY(5*24*60);

    private final int value;

    Interval(int interval)
    {
        this.value = interval;
    }

    public int getValue() {
        return value;
    }
}
