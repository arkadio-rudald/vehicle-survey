package com.arkadiusz.vehiclesurvey.model;
import java.time.LocalTime;
import java.util.Objects;

public class VehicleRecord extends SessionPeriod{
    private Direction direction;

    public VehicleRecord(LocalTime recordTimeFirstAxle, LocalTime recordTimeSecondAxle, Direction direction, Day day) {
        super(recordTimeFirstAxle,recordTimeSecondAxle,day);
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public Day getDay() {
        return super.getDay();
    }

    public LocalTime getRecordTimeFirstAxle() {
        return super.getStartTime();
    }

    public LocalTime getRecordTimeSecondAxle() {
        return super.getEndTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VehicleRecord)) return false;
        VehicleRecord that = (VehicleRecord) o;
        return Objects.equals(getRecordTimeFirstAxle(), that.getRecordTimeFirstAxle()) &&
                Objects.equals(getRecordTimeSecondAxle(), that.getRecordTimeSecondAxle()) &&
                getDirection() == that.getDirection() &&
                getDay() == that.getDay();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getRecordTimeFirstAxle(), getRecordTimeSecondAxle(), getDirection(), getDay());
    }
}
