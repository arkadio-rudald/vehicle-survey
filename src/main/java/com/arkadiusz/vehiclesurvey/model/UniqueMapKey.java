package com.arkadiusz.vehiclesurvey.model;

import com.arkadiusz.vehiclesurvey.analysis.IAnalysis;


import java.time.LocalTime;
import java.util.Objects;

public class UniqueMapKey extends SessionPeriod{
    private Direction direction;
    private Class calculation;
    private Interval interval;

    public UniqueMapKey(LocalTime startTime, LocalTime endTime, Direction direction,
                        Day day, Class<? extends IAnalysis> calculation, Interval interval) {
        super(startTime,endTime,day);
        this.direction = direction;
        this.calculation = calculation;
        this.interval = interval;
    }

    public LocalTime getStartTime() {
        return super.getStartTime();
    }

    public LocalTime getEndTime() {
        return super.getEndTime();
    }

    public Direction getDirection() {
        return direction;
    }

    public Day getDay() {
        return super.getDay();
    }

    public Class getCalculation() {
        return calculation;
    }

    public Interval getInterval() {
        return interval;
    }

    public void setCalculation(Class calculation) {
        this.calculation = calculation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniqueMapKey)) return false;
        UniqueMapKey that = (UniqueMapKey) o;
        return Objects.equals(getStartTime(), that.getStartTime()) &&
                Objects.equals(getEndTime(), that.getEndTime()) &&
                getDirection() == that.getDirection() &&
                getDay() == that.getDay() &&
                Objects.equals(getCalculation(), that.getCalculation()) &&
                getInterval() == that.getInterval();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getStartTime(), getEndTime(), getDirection(), getDay(), getCalculation(), getInterval());
    }
}

