package com.arkadiusz.vehiclesurvey.model;

public class Constants {
    public static final int NUMBER_OF_AXLES_PER_VEHICLE = 2;
    public static final double WHEEL_BASE_DISTANCE_IN_METERS = 2.5;
    public static final int SPEED_LIMIT_ON_THE_ROAD = 60;
    public static final int HOURS_IN_DAY = 24;
    public static final int MINUTES_IN_HOUR = 60;
}
