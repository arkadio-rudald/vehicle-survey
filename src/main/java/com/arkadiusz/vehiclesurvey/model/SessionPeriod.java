package com.arkadiusz.vehiclesurvey.model;

import java.time.LocalTime;

public class SessionPeriod {
    private LocalTime startTime;
    private LocalTime endTime;
    private Day day;

    public SessionPeriod(LocalTime startTime, LocalTime endTime, Day day) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public Day getDay() {
        return day;
    }
}
