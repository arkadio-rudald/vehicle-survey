package com.arkadiusz.vehiclesurvey.model;

public enum Direction {
    NORTH,
    SOUTH
}
