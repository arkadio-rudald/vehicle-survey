package com.arkadiusz.vehiclesurvey.helpers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class IOConnector {

    private boolean createDirectory(String absolutePath) throws IOException {
        Path path = Paths.get(absolutePath);

        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return true;
    }

    public void saveReportsInDirectory(String directoryAbsolutePath, List<String> fileNames, List<String> data)
    {
        try {
            if (createDirectory(directoryAbsolutePath))
                for (int i = 0; i < data.size(); i++)
                    saveToFile(directoryAbsolutePath + "/" + fileNames.get(i), data.get(i));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void saveToFile(String filePath, String data) throws IOException {
        if (!Files.exists(Paths.get(filePath))) {
            Files.createFile(Paths.get(filePath));
        }
        Files.write(Paths.get(filePath), data.getBytes());
    }

    public List<String> readFileContent(String sourceFileAbsolutePath) throws IOException {
        return Files.readAllLines(Paths.get(sourceFileAbsolutePath));
    }
}