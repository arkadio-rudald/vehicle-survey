package com.arkadiusz.vehiclesurvey.helpers;

import com.arkadiusz.vehiclesurvey.model.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class RecordParser {

    private List<String> readRecordsFromResourcesFile(String fileName)
    {
        IOConnector connector = new IOConnector();
        try {
            return connector.readFileContent(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<VehicleRecord> getVehicleRecordsFromResourceFile(String fileName)
    {
        return getVehicleRecordList(readRecordsFromResourcesFile(fileName));
    }

    private boolean isNextDay(Long previousCatTime, Long nextCarTime)
    {
        return previousCatTime > nextCarTime;
    }

    private Day changeToNextDay(Day currentDay)
    {
        return Day.values()[currentDay.getIndex() + 1];
    }

    public List<VehicleRecord> getVehicleRecordList(List<String> records)
    {
        List<VehicleRecord> vehicleRecords = new ArrayList<>();
        Day currentDay = Day.MONDAY;

        // split for 4 or 2
        List<String> singleVehicleRecords = new ArrayList<>();

        Long currentTime = 0L;
        for(int i = 0; i < records.size(); i++)
        {
            if(isNextDay(currentTime, Long.valueOf(records.get(i).substring(1, records.get(i).length()))))
                currentDay = changeToNextDay(currentDay);

            if(i%2 == 1)
            {
                Direction direction = getDirection(records.get(i-1), records.get(i));

                if (direction == Direction.NORTH)
                {
                    singleVehicleRecords.add(records.get(i-1));
                    singleVehicleRecords.add(records.get(i));
                }
                else
                {
                    singleVehicleRecords.add(records.get(i-1));
                    singleVehicleRecords.add(records.get(i));
                    singleVehicleRecords.add(records.get(i+1));
                    singleVehicleRecords.add(records.get(i+2));
                    i = i + 2;
                }
                vehicleRecords.add(getVehicleRecord(singleVehicleRecords, currentDay, direction));
                singleVehicleRecords = new ArrayList<>();
            }
            currentTime = Long.valueOf(records.get(i).substring(1, records.get(i).length()));
        }
        return vehicleRecords;
    }

    private List<SessionPeriod> getPeriodsByInterval(Interval interval)
    {
        List<SessionPeriod> sessionPeriods = new ArrayList<>();
        for(Day day : Day.values())
        {
            for (int i = 0 ; i < Constants.HOURS_IN_DAY * Constants.MINUTES_IN_HOUR / interval.getValue(); i++)
            {
                sessionPeriods.add(
                        new SessionPeriod(

                                LocalTime.ofSecondOfDay(i*interval.getValue() * 60),
                                LocalTime.ofSecondOfDay(((i+1)*interval.getValue() * 60)-1).plusNanos(999999999),
                                day
                        ));
            }
        }
        return sessionPeriods;
    }

    private void putVehicleRecordInRightSessionPeriod()
    {

    }

    public List<IntervalRecords> getVehicleRecordByInterval(Interval interval, String sourceFilePath) {
        List<IntervalRecords> resultIntervalVehicleRecordList = new ArrayList<>();
        List<VehicleRecord> parsedRecords = getVehicleRecordsFromResourceFile(sourceFilePath);

        List<SessionPeriod> periods = getPeriodsByInterval(interval);

        for (SessionPeriod period : periods)
        {
            List<VehicleRecord> vehicleRecordList = new ArrayList<>();
            for (VehicleRecord vehicleRecord : parsedRecords) {
                if (isRecordMatchingInterval(vehicleRecord, period))
                    vehicleRecordList.add(vehicleRecord);
            }
            IntervalRecords vehicleRecords = new IntervalRecords(period.getStartTime(),period.getEndTime(),period.getDay(), vehicleRecordList);
            resultIntervalVehicleRecordList.add(vehicleRecords);
        }
        return resultIntervalVehicleRecordList;
    }

    private boolean isRecordMatchingInterval(VehicleRecord vehicleRecord, SessionPeriod sessionPeriod) {
        return vehicleRecord.getRecordTimeFirstAxle().compareTo(sessionPeriod.getStartTime()) >= 0
                && vehicleRecord.getRecordTimeFirstAxle().compareTo(sessionPeriod.getEndTime()) <= 0
                && vehicleRecord.getDay() == sessionPeriod.getDay();
    }


    public VehicleRecord getVehicleRecord(List<String> recordLines, Day day, Direction direction)
    {
        LocalTime firstAxle;
        LocalTime secondAxle;

        if(direction.equals(Direction.NORTH)) {
            firstAxle = getTime(Long.valueOf(recordLines.get(0).substring(1, recordLines.get(0).length())));
            secondAxle = getTime(Long.valueOf(recordLines.get(1).substring(1,recordLines.get(1).length())));
        }
        else{
            firstAxle = getTime(Long.valueOf(recordLines.get(0).substring(1, recordLines.get(0).length())));
            secondAxle = getTime(Long.valueOf(recordLines.get(2).substring(1, recordLines.get(2).length())));
        }

        if (firstAxle != null && secondAxle != null) {
            return new VehicleRecord(
                    firstAxle,
                    secondAxle,
                    direction,
                    day
            );
        }
        else
            return null;
    }

    private Direction getDirection(String sensorRecord1, String sensorRecord2)
    {
        if (sensorRecord1.contains("A") && sensorRecord2.contains("B"))
            return Direction.SOUTH;
        else if (sensorRecord1.contains("A") && sensorRecord2.contains("A"))
            return Direction.NORTH;
        return null;
    }

    private LocalTime getTime(long time)
    {
        if (time * 1000 * 1000 > 86399999999999L)
            return null;
        return LocalTime.ofNanoOfDay(time * 1000 * 1000);
    }

}
