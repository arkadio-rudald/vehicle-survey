package com.arkadiusz.vehiclesurvey.helpers;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.AggregateCalculation;
import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.model.*;

import java.util.*;
import java.util.stream.Collectors;

public class DataCombiner{
    private RecordParser recordParser;
    private String sourceFilePath;

    public DataCombiner(RecordParser recordParser, String sourceFilePath) {
        this.recordParser = recordParser;
        this.sourceFilePath = sourceFilePath;
    }

    public Map<UniqueMapKey, Object> analyseAggregationFunctions(List<AggregateCalculation> aggregateCalculations, Map<UniqueMapKey, Object> calcMap, Interval[] intervals)
    {
        Map<UniqueMapKey, Object> results = new HashMap<>();
        for(AggregateCalculation aggregationCalc : aggregateCalculations)
        {
            Map<UniqueMapKey, Object> tempMap = calcMap.entrySet().stream().filter(
                    entry -> entry.getKey().getCalculation() == aggregationCalc.getCalculationType()
            ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            results.putAll(aggregationCalc.groupAndCalculate(tempMap, intervals, Direction.values(), getDaysFromData(calcMap)));
        }
        return results;
    }

    public List<Day> getDaysFromData(Map<UniqueMapKey, Object> map)
    {
        List<Day> dayList = new ArrayList<>();
        map.entrySet().stream().forEach(entry ->
        {
            if(!dayList.contains(entry.getKey().getDay()))
            {
                dayList.add(entry.getKey().getDay());
            }
        });
        return dayList;
    }

    public Map<UniqueMapKey, Object> iterateByIntervals(List<Interval> intervals, List<ICalculation> calculations)
    {
        Map<UniqueMapKey, Object> resultMap = new HashMap<>();
        for(Interval interval : intervals)
        {
            List<IntervalRecords> vehicleRecordByInterval = recordParser.getVehicleRecordByInterval(interval, sourceFilePath);
            for (IntervalRecords vehicleRecords : vehicleRecordByInterval)
                resultMap.putAll(iterateByAnalysis(vehicleRecords, calculations, interval));
        }
        resultMap.values().removeIf(Objects::isNull);
        return resultMap;
    }

    private Map<UniqueMapKey, Object> iterateByAnalysis(IntervalRecords vehicleRecords, List<ICalculation> calculations, Interval interval)
    {
        Map<UniqueMapKey, Object> resultMap = new HashMap<>();
        for (ICalculation calculation : calculations)
            resultMap.putAll(iterateByDirections(vehicleRecords, calculation, interval));
        return resultMap;
    }

    private Map<UniqueMapKey, Object> iterateByDirections(IntervalRecords vehicleRecords, ICalculation calculation, Interval interval)
    {
        Map<UniqueMapKey, Object> resultMap = new HashMap<>();
        for (Direction direction : Direction.values())
        {
            List<VehicleRecord> records = vehicleRecords.getVehicleRecords().stream().filter(
                    record -> record.getDirection() == direction).collect(Collectors.toList());

            resultMap.put(new UniqueMapKey(
                            vehicleRecords.getStartTime(), vehicleRecords.getEndTime(),
                            direction, vehicleRecords.getDay(), calculation.getClass(), interval),
                    calculation.calculate(records));
        }
        return resultMap;
    }
}
