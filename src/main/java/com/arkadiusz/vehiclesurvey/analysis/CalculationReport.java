package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.Interval;
import com.arkadiusz.vehiclesurvey.model.UniqueMapKey;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class CalculationReport {
    private Interval[] intervals;
    private List<Class> calculations;
    private List<Class> agregations;
    private List<Day> days;
    private Direction[] directions;

    public CalculationReport(Interval[] intervals, List<Class> calculations, List<Class> agregations, List<Day> days, Direction[] directions) {
        this.intervals = intervals;
        this.calculations = calculations;
        this.agregations = agregations;
        this.days = days;
        this.directions = directions;
    }

    public List<String> report(Map<UniqueMapKey, Object> analysisResults, Map<UniqueMapKey, Object> aggregationResults) {
        List<String> resultReports = new ArrayList<>();
        if (analysisResults != null)
            resultReports.add(getFirstLineOfReport() + iterateInterval(analysisResults));

        if(aggregationResults != null)
            resultReports.add(reportAggregationResults(aggregationResults));

        return resultReports;
    }

    private String reportAggregationResults(Map<UniqueMapKey, Object> aggregationResults)
    {
        StringBuilder result = new StringBuilder();
        for (Day day : days)
            for (Direction direction : Direction.values())
                for (Interval interval : intervals)
                    for (Class aggegationType : agregations)
                    {
                        Map<UniqueMapKey, Object> filteredMap = aggregationResults.entrySet().stream().filter(
                                entry -> entry.getKey().getDay() == day &&
                                        entry.getKey().getDirection() == direction &&
                                        entry.getKey().getInterval() == interval &&
                                        entry.getKey().getCalculation() == aggegationType
                        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                        if(filteredMap.size() > 0) {
                            UniqueMapKey key = filteredMap.entrySet().stream().findFirst().get().getKey();
                            Number value = (Number) filteredMap.entrySet().stream().filter(entry -> entry.getKey().equals(key)).findFirst().get().getValue();

                            result.append(writeAggregationReport(key, value, aggegationType));
                        }
                    }
        return result.toString();
    }

    private String writeAggregationReport(UniqueMapKey key, Number value, Class agregationType)
    {
        if (agregationType.getSimpleName().equals("PeakVolumeTime"))
            return "Max traffic on " + key.getDay() + " was between " + key.getStartTime() +
                    " - " + key.getEndTime() + " " + key.getDirection() + " direction" +
                    ". At this time passed " + value.intValue() + " cars.\n";

        return "";
    }



    private String iterateInterval(Map<UniqueMapKey, Object> map)
    {
        StringBuilder result = new StringBuilder();
        for (Interval interval : intervals)
            result.append(
                    interateDay(
                            map.entrySet().stream().filter(item -> item.getKey().getInterval() == interval)
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue))
                    )
            );

        return result.toString();
    }

    private String interateDay(Map<UniqueMapKey, Object> map) {
        StringBuilder result = new StringBuilder();
        for (Day day : days) {
            result.append(iterateDirection(map.entrySet().stream().filter(item -> item.getKey().getDay() == day)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey, Map.Entry::getValue)
                    )));
        }
        return result.toString();
    }

    private String iterateDirection(Map<UniqueMapKey, Object> map)
    {
        StringBuilder result = new StringBuilder();
        for(Direction direction : directions)
        {
            result.append(iterateCalculation(map.entrySet().stream().filter(item -> item.getKey().getDirection() == direction)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey, Map.Entry::getValue
                    ))));
        }
        return result.toString();
    }

    private String iterateCalculation(Map<UniqueMapKey, Object> map)
    {
        StringBuilder report = new StringBuilder();
        for(Map.Entry<UniqueMapKey, Object> entry : map.entrySet())
        {
            List<Object> results = new ArrayList<>();
            for (Class calculation : calculations)
            {
                Object result = map.get(new UniqueMapKey(
                        entry.getKey().getStartTime(),
                        entry.getKey().getEndTime(),
                        entry.getKey().getDirection(),
                        entry.getKey().getDay(),
                        calculation,
                        entry.getKey().getInterval()
                ));
                if (result == null)
                    results.add(0);
                else
                    results.add(result);
            }
            report.append(writeReport(entry.getKey(), results));
        }
        return report.toString();
    }

    private String writeReport(UniqueMapKey uniqueMapKey, List<Object> results)
    {
        StringBuilder line = new StringBuilder();
        line = line.append(uniqueMapKey.getInterval().getValue()).append(",")
                    .append(uniqueMapKey.getDay().name()).append(",")
                    .append(uniqueMapKey.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))).append(",")
                    .append(uniqueMapKey.getEndTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        for (Object result : results)
            line.append(",").append(result.toString());

        return line.append("\n").toString();
    }

    private String getFirstLineOfReport() {
        StringBuilder firstLine = new StringBuilder("Interval,Day,Start_Time,End_Time");
        for (Class calc : calculations)
            firstLine.append(",").append(calc.getSimpleName());
        return firstLine.append("\n").toString();
    }
}
