package com.arkadiusz.vehiclesurvey.analysis.calculation;

import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;

import java.util.List;

public class VehicleCounter implements ICalculation {

    @Override
    public Integer calculate(List<VehicleRecord> vehicleRecordList) {
        return vehicleRecordList.size();
    }

}
