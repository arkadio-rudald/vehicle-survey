package com.arkadiusz.vehiclesurvey.analysis.calculation;

import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.model.Constants;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;

public class AverageDistanceBetweenCars implements ICalculation {
    @Override
    public Double calculate(List<VehicleRecord> vehicleRecordList) {

        if(vehicleRecordList.size() < 2)
            return 0.0;
        Double totalDistance = 0.0;
        for(int i = 1; i < vehicleRecordList.size(); i++)
        {
            LocalTime t1 = vehicleRecordList.get(i).getRecordTimeFirstAxle();
            LocalTime t0 = vehicleRecordList.get(i-1).getRecordTimeSecondAxle();
            Long elapsedMiliseconds = Duration.between(t0,t1).toMillis();
            Double distance = Constants.SPEED_LIMIT_ON_THE_ROAD * 10/36 * elapsedMiliseconds.doubleValue() / 1000;
            totalDistance += distance;
        }

        return Math.floor(totalDistance / (vehicleRecordList.size() - 1) * 100) / 100;
    }
}
