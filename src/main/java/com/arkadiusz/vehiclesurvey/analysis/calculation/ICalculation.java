package com.arkadiusz.vehiclesurvey.analysis.calculation;

import com.arkadiusz.vehiclesurvey.analysis.IAnalysis;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;

import java.util.List;

public interface ICalculation extends IAnalysis {

    Object calculate(List<VehicleRecord> vehicleRecordList);
}
