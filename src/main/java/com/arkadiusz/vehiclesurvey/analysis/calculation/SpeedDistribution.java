package com.arkadiusz.vehiclesurvey.analysis.calculation;

import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.model.Constants;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;

public class SpeedDistribution implements ICalculation {
    @Override


    public Double calculate(List<VehicleRecord> vehicleRecordList)
    {
        if (vehicleRecordList.size() == 0)
            return 0.0;

        Double totalSpeed = 0.0;
        for (VehicleRecord aVehicleRecordList : vehicleRecordList) {
            LocalTime t1 = aVehicleRecordList.getRecordTimeFirstAxle();
            LocalTime t0 = aVehicleRecordList.getRecordTimeSecondAxle();
            Long elapsedMiliseconds = Duration.between(t1, t0).toMillis();
            Double elapsedSeconds = elapsedMiliseconds.doubleValue()/1000;
            Double speed = Constants.WHEEL_BASE_DISTANCE_IN_METERS / elapsedSeconds * 36 / 10;
            totalSpeed += speed;
        }

        return Math.floor(totalSpeed/vehicleRecordList.size() * 100) / 100;
    }

}
