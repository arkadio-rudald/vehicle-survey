package com.arkadiusz.vehiclesurvey.analysis.calculation;

import java.util.ArrayList;
import java.util.List;

public class Analysis {
    private List<ICalculation> calculation;

    public List<ICalculation> getCalculation() {
        return calculation;
    }

    private Analysis(AnalysisBuilder analysisBuilder) {
        calculation = new ArrayList<>();

        addAverageDistanceCalculation(analysisBuilder.averageDistanceBetweenCars);
        addSpeedDistribution(analysisBuilder.speedDistribution);
        addVehicleCounter(analysisBuilder.vehicleCounter);
    }

    private void addAverageDistanceCalculation(boolean isAverageDistanceAdded)
    {
        if (isAverageDistanceAdded)
            calculation.add(new AverageDistanceBetweenCars());
    }

    private void addSpeedDistribution(boolean isSpeedDistributionAdded)
    {
        if (isSpeedDistributionAdded)
            calculation.add(new SpeedDistribution());
    }

    private void addVehicleCounter(boolean isVehicleCounterAdded)
    {
        if (isVehicleCounterAdded)
            calculation.add(new VehicleCounter());
    }

    public static class AnalysisBuilder {
        private boolean averageDistanceBetweenCars;
        private boolean vehicleCounter;
        private boolean speedDistribution;

        public AnalysisBuilder(boolean averageDistanceBetweenCars, boolean vehicleCounter, boolean speedDistribution) {
            this.averageDistanceBetweenCars = averageDistanceBetweenCars;
            this.vehicleCounter = vehicleCounter;
            this.speedDistribution = speedDistribution;
        }

        public Analysis build()
        {
            return new Analysis(this);
        }
    }
}
