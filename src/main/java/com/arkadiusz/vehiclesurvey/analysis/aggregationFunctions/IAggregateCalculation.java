package com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions;

import com.arkadiusz.vehiclesurvey.analysis.IAnalysis;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.Interval;
import com.arkadiusz.vehiclesurvey.model.UniqueMapKey;

import java.util.List;
import java.util.Map;

public interface IAggregateCalculation extends IAnalysis {

    Map<UniqueMapKey, Number> groupAndCalculate(Map<UniqueMapKey, Object> calcMap, Interval[] intervals, Direction[] directions, List<Day> days);
}
