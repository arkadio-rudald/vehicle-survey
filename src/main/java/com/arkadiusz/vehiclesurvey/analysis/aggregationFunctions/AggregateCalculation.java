package com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions;

public abstract class AggregateCalculation implements IAggregateCalculation {

    private Class calculationType;

    protected AggregateCalculation(Class calculationType) {
        this.calculationType = calculationType;
    }

    public Class getCalculationType() {
        return calculationType;
    }
}
