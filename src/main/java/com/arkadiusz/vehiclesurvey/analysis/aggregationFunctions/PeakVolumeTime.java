package com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.AggregateCalculation;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.Interval;
import com.arkadiusz.vehiclesurvey.model.UniqueMapKey;


import java.util.*;
import java.util.stream.Collectors;

public class PeakVolumeTime extends AggregateCalculation {

    public PeakVolumeTime(Class calculationType) {
        super(calculationType);
    }

    private Double calculate(List<Double> calculatedRecords) {
        Optional<Double> max = calculatedRecords.stream().reduce(Double::max);
        return max.orElse(0.0);
    }

    @Override
    public Map<UniqueMapKey, Number> groupAndCalculate(Map<UniqueMapKey, Object> calcMap, Interval[] intervals, Direction[] directions, List<Day> days)
    {
        Map<UniqueMapKey, Number> results = new HashMap<>();

        for (Interval interval : intervals)
        {
            for (Direction direction : directions)
            {
                for (Day day : days)
                {
                    Map<UniqueMapKey, Object> filteredMap = calcMap.entrySet().stream().filter(
                            entry -> {
                                    entry.getKey().setCalculation(this.getClass());
                                    return entry.getKey().getDay() == day &&
                                            entry.getKey().getDirection() == direction &&
                                            entry.getKey().getInterval() == interval;
                            }
                    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                    List<Double> groupedValues = new ArrayList<>();
                    filteredMap.values().forEach(item -> groupedValues.add(
                            item instanceof Integer ? Double.valueOf(((Integer) item).doubleValue()) : (Double)item));

                    Double result = calculate(groupedValues);

                    filteredMap.entrySet().stream().filter(
                            entry -> {
                                Double value = entry.getValue() instanceof Integer ? Double.valueOf(((Integer) entry.getValue()).doubleValue()) : (Double)entry.getValue();
                                return value.equals(result);
                            })
                            .map(Map.Entry::getKey).collect(Collectors.toSet())
                            .forEach(item -> results.put(item, result));
                }
            }
        }
        return results;
    }
}
