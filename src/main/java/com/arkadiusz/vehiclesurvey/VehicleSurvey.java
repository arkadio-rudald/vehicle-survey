package com.arkadiusz.vehiclesurvey;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.AggregateCalculation;
import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.PeakVolumeTime;
import com.arkadiusz.vehiclesurvey.analysis.calculation.Analysis;
import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.analysis.calculation.VehicleCounter;
import com.arkadiusz.vehiclesurvey.model.Interval;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class VehicleSurvey {
    public static void main(String args[])
    {
        VehicleSurveyController vehicleSurveyController = new VehicleSurveyController("C:\\Users\\Dorota\\vehicle-survey\\src\\test\\resources\\data.txt");

        List<ICalculation> iCalculations = new Analysis.AnalysisBuilder(
                true, true, true
        ).build().getCalculation();

        List<AggregateCalculation> aggregateCalculations = new ArrayList<>();
        aggregateCalculations.add(new PeakVolumeTime(VehicleCounter.class));

        List<String> fileNames = new ArrayList<>();
        fileNames.add("data_report.csv");
        fileNames.add("aggregation_functions.csv");

        //default to file named analysis, full analysis
        vehicleSurveyController.startAnalyzing(Interval.values(), iCalculations, aggregateCalculations, "/directory/",fileNames);
    }
}
