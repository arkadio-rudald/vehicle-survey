package com.arkadiusz.vehiclesurvey;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.AggregateCalculation;
import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.PeakVolumeTime;
import com.arkadiusz.vehiclesurvey.analysis.calculation.Analysis;
import com.arkadiusz.vehiclesurvey.analysis.calculation.ICalculation;
import com.arkadiusz.vehiclesurvey.analysis.calculation.VehicleCounter;
import com.arkadiusz.vehiclesurvey.helpers.IOConnector;
import com.arkadiusz.vehiclesurvey.model.Interval;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class VehicleSurveyControllerTest {

    public final static String BASE_PATH = "C:\\Users\\Dorota\\vehicle-survey\\";

    @Test
    void shouldReturnCorrectReportFromGivenData()
    {
        VehicleSurveyController controller = new VehicleSurveyController(BASE_PATH + "test_sample_data.txt");

        List<ICalculation> iCalculations = new Analysis.AnalysisBuilder(
                 true, true, true
        ).build().getCalculation();

        File f = new File(BASE_PATH + "\\analysis\\data_report.csv");

        List<AggregateCalculation> aggregateCalculations = new ArrayList<>();
        aggregateCalculations.add(new PeakVolumeTime(VehicleCounter.class));

        List<String> fileNames = new ArrayList<>();
        fileNames.add("data_report.csv");
        fileNames.add("aggregation_functions.txt");

        controller.startAnalyzing(Interval.values(), iCalculations, aggregateCalculations,
                BASE_PATH + "\\analysis\\", fileNames);

        assertEquals(true, f.exists());
        assertEquals(true, !f.isDirectory());

        IOConnector ioConnector = new IOConnector();

        try {
            assertEquals(7291,
                    ioConnector.readFileContent(BASE_PATH + "\\analysis\\data_report.csv").size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}