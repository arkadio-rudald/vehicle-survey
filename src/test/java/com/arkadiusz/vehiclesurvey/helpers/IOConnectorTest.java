package com.arkadiusz.vehiclesurvey.helpers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.arkadiusz.vehiclesurvey.VehicleSurveyControllerTest.BASE_PATH;
import static org.junit.jupiter.api.Assertions.*;

class IOConnectorTest {
    private IOConnector ioConnector;


    @BeforeEach
    void initializeIOConnector()
    {
        ioConnector = new IOConnector();
    }

    @Test
    void shouldSaveFile()
    {
        File f = new File(BASE_PATH + "save_result.txt");
        try {
            ioConnector.saveToFile(BASE_PATH + "save_result.txt", "data");
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(true, f.exists());
        assertEquals(true, !f.isDirectory());

        List<String> expected = new ArrayList<>();
        expected.add("data");
        try {
            assertEquals(expected, ioConnector.readFileContent(BASE_PATH + "save_result.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void shouldCreateDirectoryAndSaveFiles()
    {
        List<String> testReports = new ArrayList<>();
        testReports.add("test1");
        testReports.add("test2");
        List<String> names = new ArrayList<>();
        names.add("test1.csv");
        names.add("test2.csv");

        ioConnector.saveReportsInDirectory("directory/check/", names, testReports);

        File d = new File("directory/check/");
        assertEquals(true, d.isDirectory());
        File[] filesInDirectory = d.listFiles();

        assert filesInDirectory != null;
        assertEquals(2,filesInDirectory.length);

    }

}