package com.arkadiusz.vehiclesurvey.helpers;

import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RecordParserTest {
    private RecordParser recordParser;

    @BeforeEach
    void initializeRecordParser()
    {
        recordParser = new RecordParser();
    }

    @Test
    void shouldReturnSingleVehicleRecord()
    {
        List<String> sensorRecords = new ArrayList<>();
        sensorRecords.add("A268981");
        sensorRecords.add("A269123");

        assertEquals(new VehicleRecord(
                LocalTime.of(0, 4, 28,981 * 1000 * 1000),
                LocalTime.of(0, 4, 29,123 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY
        ),recordParser.getVehicleRecord(sensorRecords, Day.MONDAY, Direction.NORTH));

    }

    @Test
    void shouldReturnSingleVehicleRecordInSouthDirection()
    {
        List<String> sensorRecords = new ArrayList<>();
        sensorRecords.add("A604957");
        sensorRecords.add("B604960");
        sensorRecords.add("A605128");
        sensorRecords.add("B605132");

        assertEquals(new VehicleRecord(
                LocalTime.of(0, 10, 4,957 * 1000 * 1000),
                LocalTime.of(0, 10, 5,128 * 1000 * 1000),
                Direction.SOUTH,
                Day.MONDAY
        ),recordParser.getVehicleRecord(sensorRecords, Day.MONDAY, Direction.SOUTH));

    }

    @Test
    void shouldReturnNullMaxMilisecondsCheck()
    {
        List<String> sensorRecords = new ArrayList<>();
        sensorRecords.add("A86400000");
        sensorRecords.add("A86410000");
        assertEquals(null,recordParser.getVehicleRecord(sensorRecords, Day.MONDAY, Direction.NORTH));
    }


}