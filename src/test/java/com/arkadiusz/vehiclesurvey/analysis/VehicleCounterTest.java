package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.analysis.calculation.VehicleCounter;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class VehicleCounterTest {
    private List<VehicleRecord> vehicleRecordList;
    private VehicleCounter vehicleCounter;

    /*
        1. the same times in the same direction
        2. normal count
     */

    @BeforeEach
    void initializeVehicleCounter()
    {
        vehicleCounter = new VehicleCounter();
    }

    private void addTestData()
    {
        vehicleRecordList = new ArrayList<>();
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,0,0,8 * 1000 * 1000),
                LocalTime.of(1,0,0,258 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));

        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,1,0,8 * 1000 * 1000),
                LocalTime.of(1,1,0,133 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));

        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,2,0,8 * 1000 * 1000),
                LocalTime.of(1,2,0,108 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
    }

    @Test
    void countVehicles()
    {
        addTestData();
        assertEquals(Integer.valueOf(3), vehicleCounter.calculate(vehicleRecordList));
    }

    @Test
    void countVehiclesWhichCameAtTheSameTime()
    {
        vehicleRecordList = new ArrayList<>();
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,0,0,0),
                LocalTime.of(1,0,0,0),
                Direction.NORTH,
                Day.MONDAY));

        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,1,0,0),
                LocalTime.of(1,1,0,0),
                Direction.NORTH,
                Day.MONDAY));

        assertEquals(Integer.valueOf(2), vehicleCounter.calculate(vehicleRecordList));
    }
}