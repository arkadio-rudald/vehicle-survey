package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.analysis.calculation.SpeedDistribution;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SpeedDistributionTest {
    private List<VehicleRecord> vehicleRecordList;
    private SpeedDistribution speedDistribution;

    /*
        SpeedDistribution input data, and you get
        avg speed at this road,
        but it could be included time interval there

     */

    @BeforeEach
    void initializeSpeedDistribution()
    {
        speedDistribution = new SpeedDistribution();
    }


    @Test
    void shouldGiveAverageSpeedInTheSpecificPeriod()
    {
        vehicleRecordList = new ArrayList<>();
        // 10m/s
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,3,0,8 * 1000 * 1000),
                LocalTime.of(1,3,0,258 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));

        // 20m/s
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(2,3,0,8 * 1000 * 1000),
                LocalTime.of(2,3,0,133 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        // 25 m/s
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,15,0,8 * 1000 * 1000),
                LocalTime.of(1,15,0,108 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));

        // 10 m/s
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(2,15,0,8 * 1000 * 1000),
                LocalTime.of(2,15,0,258 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));

        //km/h
        assertEquals(Double.valueOf(58.5),speedDistribution.calculate(vehicleRecordList));
    }

    @Test
    void shouldGiveZeroAsAResult()
    {
        vehicleRecordList = new ArrayList<>();
        assertEquals(Double.valueOf(0.0),speedDistribution.calculate(vehicleRecordList));
    }

}