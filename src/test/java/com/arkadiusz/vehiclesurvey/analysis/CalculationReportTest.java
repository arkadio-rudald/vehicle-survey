package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.PeakVolumeTime;
import com.arkadiusz.vehiclesurvey.analysis.calculation.SpeedDistribution;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.Interval;
import com.arkadiusz.vehiclesurvey.model.UniqueMapKey;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CalculationReportTest {
    private Map<UniqueMapKey, Object> map;


    @Test
    void shouldReturnValidReportOnlyCalculations()
    {
        map = new HashMap<>();
        map.put(new UniqueMapKey(
                        LocalTime.of(0,0,0,0),
                        LocalTime.of(0, 14, 59, 999999999),
                        Direction.NORTH,
                        Day.MONDAY,
                        SpeedDistribution.class,
                        Interval.FIFTEEN_MINUTES
                ), 30.0
        );
        List<Class> iCalculations = new ArrayList<>();
        iCalculations.add(SpeedDistribution.class);

        List<Day> days = new ArrayList<>();
        days.add(Day.MONDAY);

        CalculationReport calculationReport = new CalculationReport(
                Interval.values(), iCalculations,null, days, Direction.values());


        List<String> reports = new ArrayList<>();
        String expectedReport = "Interval,Day,Start_Time,End_Time,SpeedDistribution\n" +
                "15,MONDAY,00:00:00,00:14:59,30.0\n";
        reports.add(expectedReport);
        List<String> actual = calculationReport.report(map,null);


        assertEquals(reports, actual);
    }

    @Test
    void shouldReturnValidReportAggregation()
    {
        map = new HashMap<>();
        map.put(new UniqueMapKey(
                        LocalTime.of(0,0,0,0),
                        LocalTime.of(0, 14, 59, 999999999),
                        Direction.NORTH,
                        Day.MONDAY,
                        PeakVolumeTime.class,
                        Interval.FIFTEEN_MINUTES
                ), 5.0
        );
        List<Class> agregations = new ArrayList<>();
        agregations.add(PeakVolumeTime.class);

        List<Day> days = new ArrayList<>();
        days.add(Day.MONDAY);

        CalculationReport calculationReport = new CalculationReport(
                Interval.values(), null, agregations, days, Direction.values());

        List<String> reports = new ArrayList<>();
        String expectedReport = "Max traffic on MONDAY was between 00:00 - 00:14:59.999999999 " +
                "NORTH direction. At this time passed 5 cars.\n";
        reports.add(expectedReport);
        List<String> actual = calculationReport.report(null,map);
        assertEquals(reports, actual);
    }
}