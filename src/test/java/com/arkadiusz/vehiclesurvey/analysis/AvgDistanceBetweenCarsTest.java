package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.analysis.calculation.AverageDistanceBetweenCars;
import com.arkadiusz.vehiclesurvey.model.Day;
import com.arkadiusz.vehiclesurvey.model.Direction;
import com.arkadiusz.vehiclesurvey.model.VehicleRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AvgDistanceBetweenCarsTest {
    private List<VehicleRecord> vehicleRecordList;
    private AverageDistanceBetweenCars avgDistanceBetweenCars;

    // corner cases
    /*
        1. When car is just behind the other one
     */


    @BeforeEach
    void initializeAverageDistanceBetweenCarsObject()
    {
        avgDistanceBetweenCars = new AverageDistanceBetweenCars();
    }

    private void correctTestData()
    {
        vehicleRecordList = new ArrayList<>();
        // 10 m/s, 250 ms

        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,0,0,0 * 1000 * 1000),
                LocalTime.of(1,0,0,250 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        // 20 m/s, 125 ms
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,1,0,0 * 1000 * 1000),
                LocalTime.of(1,1,0,125 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        //25 m/s , 100 ms
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,3,0,0 * 1000 * 1000),
                LocalTime.of(1,3,0,100 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        // 10m/s
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,6,0,0 * 1000 * 1000),
                LocalTime.of(1,6,0,250 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        //20ms
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,12,0,0 * 1000 * 1000),
                LocalTime.of(1,12,0,125 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
        //25ms
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,24,0,0 * 1000 * 1000),
                LocalTime.of(1,24,0,100 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY));
    }



    @Test
    void calculationTestWithCorrectData() {
        correctTestData();
        //calculate V of the current car,
        // 2,5 / t_second_axle-t_first_axle =  V
        //s = V * (t_second_car_first_axle-t_first_car_second_axle)

        // 1-2 = 600m
        // 2-3 = 1200m
        // 3-4 = 1500m
        // 4-5 = 600m
        // 5-6 = 1200m

        assertEquals(Double.valueOf(4605.28), avgDistanceBetweenCars.calculate(vehicleRecordList));
    }


    @Test
    void calculateWhenCarsAreClose()
    {
        vehicleRecordList = new ArrayList<>();
        // 10 m/s, 250 ms
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,0,0,8 * 1000 * 1000),
                LocalTime.of(1,0,0,258 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY)
        );
        // 10 m/s,
        vehicleRecordList.add(new VehicleRecord(
                LocalTime.of(1,0,0,259 * 1000 * 1000),
                LocalTime.of(1,0,0,509 * 1000 * 1000),
                Direction.NORTH,
                Day.MONDAY)
        );

        assertEquals(Double.valueOf(0.01), avgDistanceBetweenCars.calculate(vehicleRecordList));
    }
}