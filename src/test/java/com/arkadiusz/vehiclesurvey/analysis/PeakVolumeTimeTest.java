package com.arkadiusz.vehiclesurvey.analysis;

import com.arkadiusz.vehiclesurvey.analysis.aggregationFunctions.PeakVolumeTime;
import com.arkadiusz.vehiclesurvey.analysis.calculation.VehicleCounter;
import com.arkadiusz.vehiclesurvey.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PeakVolumeTimeTest {
    private Map<UniqueMapKey, Object> calculatedRecords;
    private PeakVolumeTime peakVolumeTime;


    /*
        I get all day session and I select peakVolumeTime;
        when the biggest number of cars passes
        it returns hour

        1. whether it returns the max number,
        2. what returns when the number is the same at 2 hours,
        3. what if no car passed.
     */

    @BeforeEach
    void initializePeakVolumeTime() {
        peakVolumeTime = new PeakVolumeTime(VehicleCounter.class);
    }


    @Test
    void calculatePeakVolumeTime() {
        calculatedRecords = new HashMap<>();
        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.MONDAY,
                        VehicleCounter.class,
                        Interval.HOUR
                ),
                200.0
        );

        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.MONDAY,
                        VehicleCounter.class,
                        Interval.HOUR
                ),
                150.0
        );

        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        VehicleCounter.class,
                        Interval.HOUR
                ),
                200.0
        );

        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                200.0
        );
        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.SOUTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                200.0
        );

        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.SOUTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                205.0
        );
        //should be an hour and a number of cars

        Map<UniqueMapKey, Number> expectedMap = new HashMap<>();
        expectedMap.put(new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.MONDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                200.0);

        expectedMap.put(new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                200.0);

        expectedMap.put(new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                200.0);

        expectedMap.put(new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.SOUTH,
                        Day.TUESDAY,
                        PeakVolumeTime.class,
                        Interval.HOUR
                ),
                205.0);


        Interval[] intervals = {Interval.HOUR};

        List<Day> days = new ArrayList<>();
        days.add(Day.MONDAY);
        days.add(Day.TUESDAY);

        Map<UniqueMapKey, Number> actualMap = peakVolumeTime.groupAndCalculate(
                calculatedRecords, intervals, Direction.values(), days);

        assertEquals(expectedMap,actualMap);
    }

    @Test
    void theSameNumberOfCarsAtDifferentHours() {
        calculatedRecords = new HashMap<>();
        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(0, 0),
                        LocalTime.of(0, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        VehicleCounter.class,
                        Interval.HOUR
                ),
                200.0
        );

        calculatedRecords.put(
                new UniqueMapKey(
                        LocalTime.of(1, 0),
                        LocalTime.of(1, 59, 59, 999999999),
                        Direction.NORTH,
                        Day.TUESDAY,
                        VehicleCounter.class,
                        Interval.HOUR
                ),
                200.0
        );

        Interval[] intervals = {Interval.HOUR};
        List<Day> days = new ArrayList<>();
        days.add(Day.TUESDAY);

        Map<UniqueMapKey, Number> actualMap = peakVolumeTime.groupAndCalculate(
                calculatedRecords, intervals, Direction.values(), days);
        assertEquals(calculatedRecords, actualMap);
    }

    @Test
    void noCarPassed()
    {
        calculatedRecords = new HashMap<>();
        Interval[] intervals = {Interval.HOUR};

        List<Day> days = new ArrayList<>();

        Map<UniqueMapKey, Number> actualMap = peakVolumeTime.groupAndCalculate(
                calculatedRecords, intervals, Direction.values(), days);
        assertEquals(new HashMap<>(), actualMap);
    }
}