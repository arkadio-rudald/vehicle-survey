# README #

### INSTALLATION PROCESS ###

1. Make sure you have installed JDK (In submission I was using JDK 1.8.0_151)
2. Please open your IDE and try to get source using GIT
3. Link to the source: https://bitbucket.org/arkadio-rudald/toy-robot-simulator
Personally I recommend using IntelliJ because it natively supports Gradle.

For IntelliJ:

Check out from the VCS. When wizard asks about choosing Gradle distribution I recommend using Gradle Wrapper in this case.
Find the terminal in Intellij and type:
gradlew build - to build solution and execute tests,

### IMPORTANT ###

Before code and test execution please set up 2 variables:

ABSOLUTE_PATH in VehicleSurvey.java

BASE_PATH in VehicleSurveyControllerTest.java

